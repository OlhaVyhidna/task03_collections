package collections.priorityqueue;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyPriorityQueue<E extends Comparable<E>> extends AbstractQueue<E> {

  private static Logger logger = LogManager.getLogger(MyPriorityQueue.class);
  private List<E> queue;

  public MyPriorityQueue() {
    queue = new ArrayList<>();
  }


  @Override
  public Iterator<E> iterator() {
    return queue.iterator();
  }

  @Override
  public int size() {
    return queue.size();
  }

  @Override
  public boolean offer(E e) {
    return add(e);
  }

  @Override
  public E poll() {
    if (queue.isEmpty()) {
      logger.warn("Queue is empty");
      return null;
    }
    E polledElem = queue.get(0);
    queue.remove(polledElem);
    logger.info("Element was polled");
    return polledElem;
  }

  @Override
  public E peek() {
    if (queue.isEmpty()) {
      logger.warn("Queue is empty. Peek method");
      return null;
    }
    E peekedElement =queue.get(0);
    logger.info("Element " + peekedElement + " was peeked");
    return peekedElement;
  }

  @Override
  public boolean add(E e) {
    if (e == null) {
      logger.error("In method add was inputted null");
      throw new NullPointerException();
    }
    queue.add(e);
    logger.info("Element " + e + " was inserted");
    Collections.sort(queue);
    return true;
  }

  @Override
  public E remove() {
    if(queue.isEmpty()){
      logger.warn("Queue is empty. Remove by object method");
      return null;
    }
    E removedElem = queue.get(0);
    this.remove(removedElem);
    return removedElem;
  }

  @Override
  public boolean remove(Object o) {
    int index = queue.indexOf(o);
    if (index==-1){
      logger.warn("Queue does not contain inputted object");
      return false;
    }
    queue.remove(o);
    logger.info("Element " + o + " was removed");
    return true;
  }

}
