package collections.deque;

import collections.priorityqueue.MyPriorityQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyDeque<E extends Comparable<E>> implements Deque<E> {

  private static Logger logger = LogManager.getLogger(MyDeque.class);
  private List<E> deque;

  public MyDeque() {
    deque = new ArrayList<>();
  }

  @Override
  public void addFirst(E e) {
    offerFirst(e);
  }

  @Override
  public void addLast(E e) {
    offerFirst(e);

  }

  @Override
  public boolean offerFirst(E e) {
    if (e == null) {
      logger.error("In method was inputted null");
      throw new NullPointerException();
    }
    if (deque.isEmpty()) {
      deque.add(e);
    } else {
      deque.add(size(), e);
    }
    logger.info("Element " + e + " was inserted in the beginning of container");
    return true;
  }

  @Override
  public boolean offerLast(E e) {
    if (e == null) {
      logger.error("In method was inputted null");
      throw new NullPointerException();
    }
    deque.add(e);
    logger.info("Element " + e + " was inserted in the end of container");
    return true;
  }

  @Override
  public E removeFirst() {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty. RemoveFirst method");
      throw new NoSuchElementException();
    }
    E firstElement = deque.get(0);
    remove(firstElement);
    return firstElement;
  }

  @Override
  public E removeLast() {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty. RemoveLast method");
      throw new NoSuchElementException();
    }
    E lastElement = deque.get(size() - 1);
    remove(lastElement);
    return lastElement;
  }

  @Override
  public E pollFirst() {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty. PoolFirst method");
      return null;
    }
    E firstElement = deque.get(0);
    remove(firstElement);
    return firstElement;
  }

  @Override
  public E pollLast() {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty. PoolLast method");
      return null;
    }
    E lastElement = deque.get(size() - 1);
    remove(lastElement);
    return lastElement;
  }

  @Override
  public E getFirst() {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty. GetFirst method");
      throw new NoSuchElementException();
    }
    return deque.get(0);
  }

  @Override
  public E getLast() {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty. GetLast method");
      throw new NoSuchElementException();
    }
    return deque.get(size() - 1);
  }

  @Override
  public E peekFirst() {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty. PeekFirst method");
      return null;
    }
    return deque.get(0);
  }

  @Override
  public E peekLast() {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty. PeekLast method");
      return null;
    }
    return deque.get(size() - 1);
  }

  @Override
  public boolean removeFirstOccurrence(Object o) {
    if (!deque.contains(o)) {
      return false;
    }
    deque.remove(o);
    return true;
  }

  @Override
  public boolean removeLastOccurrence(Object o) {
    if (!deque.contains(o)) {
      return false;
    }
    int lastIndexOf = deque.lastIndexOf(o);
    deque.remove(lastIndexOf);
    return true;
  }

  @Override
  public boolean add(E e) {
    return offerLast(e);
  }

  @Override
  public boolean offer(E e) {
    return offerLast(e);
  }

  @Override
  public E remove() {
    return removeFirst();
  }

  @Override
  public E poll() {
    return peekFirst();
  }

  @Override
  public E element() {
    return getFirst();
  }

  @Override
  public E peek() {
    return peekFirst();
  }

  @Override
  public void push(E e) {
    addFirst(e);
  }

  @Override
  public E pop() {
    return removeFirst();
  }

  @Override
  public boolean remove(Object o) {
    if (deque.isEmpty()) {
      logger.warn("Queue is empty.\n Remove by object method");
      throw new NoSuchElementException();
    }
    if (o == null) {
      logger.error("In remove method was inputted null");
      throw new NullPointerException();
    }
    deque.remove(o);
    return true;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return deque.containsAll(c);
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    deque = new ArrayList<>();
    return true;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {
    deque = new ArrayList<>();
  }

  @Override
  public boolean contains(Object o) {
    return deque.contains(o);
  }

  @Override
  public int size() {
    return deque.size();
  }

  @Override
  public boolean isEmpty() {
    return deque.isEmpty();
  }

  @Override
  public Iterator<E> iterator() {
    return deque.iterator();
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public Iterator<E> descendingIterator() {
    return null;
  }

  public List<E> getDeque() {
    return deque;
  }
}
