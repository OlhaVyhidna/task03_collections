package collections;

import collections.treemap.MyTreeMap;

public class Main {

  public static void main(String[] args) {
    MyTreeMap<Integer, Integer> integerMyTreeMap = new MyTreeMap<>();
    integerMyTreeMap.put(16, 5);
    integerMyTreeMap.put(4, 9);
    integerMyTreeMap.put(20, 9);
    integerMyTreeMap.put(13, 11);
    integerMyTreeMap.put(10, 11);
    integerMyTreeMap.put(7, 11);
    integerMyTreeMap.put(15, 11);
    integerMyTreeMap.put(17, 11);
    integerMyTreeMap.put(8, 19);
    integerMyTreeMap.put(11, 19);
    integerMyTreeMap.put(23, 19);
    integerMyTreeMap.put(21, 19);
    integerMyTreeMap.put(25, 19);
    integerMyTreeMap.put(22, 19);
    integerMyTreeMap.put(18, 19);
    integerMyTreeMap.printTree(integerMyTreeMap.getRoot());
    System.out.println();
    integerMyTreeMap.remove(16);
    integerMyTreeMap.printTree(integerMyTreeMap.getRoot());
    System.out.println();
  }
}
