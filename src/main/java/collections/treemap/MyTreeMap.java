package collections.treemap;

import collections.deque.MyDeque;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyTreeMap<K extends Comparable<K>, V> implements Map<K, V> {

  private static Logger logger = LogManager.getLogger(MyDeque.class);
  private int size;
  private Node<K, V> root;

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public boolean containsKey(Object key) {
    if (key == null || root == null) {
      logger.error("NullPointerException at put method");
      throw new NullPointerException();
    }
    return findNodeByKey(key, root) != null;
  }

  @Override
  public boolean containsValue(Object value) {
    return false;
  }

  @Override
  public V get(Object key) {
    if (key == null || root == null) {
      logger.error("NullPointerException at put method");
      throw new NullPointerException();
    }
    Node<K, V> nodeByKey = findNodeByKey(key, root);
    if (nodeByKey != null) {
      return nodeByKey.value;
    }
    return null;
  }

  private Node<K, V> findNodeByKey(Object key, Node<K, V> currentNode) {
    K kKey = (K) key;
    K currentKey = currentNode.getKey();
    Node<K, V> rightChild = currentNode.getRightChild();
    Node<K, V> leftChild = currentNode.getLeftChild();
    Node<K, V> result = null;
    if (currentKey.compareTo(kKey) == 0) {
      logger.info("Node was found");
      result = currentNode;
    } else if (currentKey.compareTo(kKey) < 0) {
      if (rightChild != null) {
        result = findNodeByKey(key, currentNode.getRightChild());
      }
    } else if (leftChild != null) {
      result = findNodeByKey(key, currentNode.getLeftChild());
    }
    return result;
  }

  @Override
  public V put(K key, V value) {
    if (key == null || value == null) {
      logger.error("NullPointerException at put method");
      throw new NullPointerException();
    }
    if (root == null) {
      root = new Node<>(key, value, null);
      logger.info("Initialized root node");
      return null;
    } else {
      return insertInMap(key, value, root);
    }
  }

  private V insertInMap(K key, V value, Node<K, V> currentNode) {
    K currentKey = currentNode.getKey();
    if (currentKey.compareTo(key) == 0) {
      V oldValue = currentNode.getValue();
      currentNode.setValue(value);
      logger.info("Changed existed value");
      return oldValue;
    } else if (currentKey.compareTo(key) < 0) {
      Node<K, V> rightChild = currentNode.getRightChild();
      if (rightChild != null) {
        insertInMap(key, value, rightChild);
      } else {
        currentNode.setRightChild(new Node<>(key, value, currentNode));
        logger.info("Created right child");
        size++;
        return null;
      }
    } else {
      Node<K, V> leftChild = currentNode.getLeftChild();
      if (leftChild != null) {
        insertInMap(key, value, leftChild);
      } else {
//        leftChild = new Node<>(key, value, currentNode);
        currentNode.setLeftChild(new Node<>(key, value, currentNode));
        logger.info("Created left child");
        size++;
        return null;
      }
    }
    return null;
  }

  @Override
  public V remove(Object key) {
    K kKey = (K) key;
    Node<K, V> nodeByKey = findNodeByKey(key, root);
    if (nodeByKey == null) {
      logger.error("Binary tree does not contain element with this key");
      throw new NullPointerException();
    }
    Node<K, V> nodeByKeyRightChild = nodeByKey.getRightChild();
    Node<K, V> nodeByKeyLeftChild = nodeByKey.getLeftChild();
    Node<K, V> nodeByKeyParent = nodeByKey.getParent();
    if (nodeByKeyRightChild != null && nodeByKeyLeftChild != null) {
      removeIfNodeHasBothChildren(nodeByKey);
    } else if (nodeByKeyLeftChild != null) {
      if (nodeByKeyParent == null) {
        root = nodeByKeyLeftChild;
      } else {
        nodeByKeyLeftChild.setParent(nodeByKeyParent);
        if (nodeByKeyParent.getKey().compareTo(nodeByKey.getKey())<0){
          nodeByKeyParent.setRightChild(nodeByKeyLeftChild);
        }else {
          nodeByKeyParent.setLeftChild(nodeByKeyLeftChild);
        }
      }
    } else if (nodeByKeyRightChild != null) {
      if (nodeByKeyParent == null) {
        root = nodeByKeyRightChild;
      } else {
        nodeByKeyRightChild.setParent(nodeByKeyParent);
        if (nodeByKeyParent.getKey().compareTo(nodeByKey.getKey())<0) {
          nodeByKeyParent.setRightChild(nodeByKeyRightChild);
        }else {
          nodeByKeyParent.setLeftChild(nodeByKeyRightChild);
        }
      }
    } else {
      removeIfDontHaveChildren(nodeByKey);
    }
    size--;
    return nodeByKey.getValue();
  }

  private void removeIfDontHaveChildren(Node<K, V> currentNode) {
    Node<K, V> parentNode = currentNode.getParent();
    if (parentNode == null) {
      currentNode = null;
    }
    K parentKey = parentNode.getKey();
    K currentKey = currentNode.getKey();
    if (parentKey.compareTo(currentKey) < 0) {
      parentNode.setRightChild(null);
    } else {
      parentNode.setLeftChild(null);
    }
  }

  private void removeIfNodeHasBothChildren(Node<K, V> currentNode) {
    Node<K, V> heirNode = findHeir(currentNode.getRightChild());
    Node<K, V> heirNodeRightChild = heirNode.getRightChild();
    Node<K, V> currentNodeParent = currentNode.getParent();

    if (heirNodeRightChild != null) {
      if (heirNode.getParent().getKey().compareTo(heirNode.getKey())<0){
        heirNode.getParent().setRightChild(heirNodeRightChild);
      }else {
        heirNode.getParent().setLeftChild(heirNodeRightChild);
      }
    } else {
      if (heirNode.getParent().getKey().compareTo(heirNode.getKey())<0){
        heirNode.getParent().setRightChild(null);
      }else {
        heirNode.getParent().setLeftChild(null);
      }
    }

    if (currentNodeParent!=null){
      if (currentNodeParent.getKey().compareTo(currentNode.getKey())<0){
        currentNodeParent.setRightChild(heirNode);
      }else currentNodeParent.setLeftChild(heirNode);
      heirNode.setParent(currentNodeParent);
      heirNode.setLeftChild(currentNode.getLeftChild());
      heirNode.setRightChild(currentNode.getRightChild());
    }else {
      root.setValue(heirNode.getValue());
      root.setKey(heirNode.getKey());
    }
  }

  // the heir have to be the last left child of right child of deleted element
  private Node<K, V> findHeir(Node<K, V> currentNode) {
    Node<K, V> result = currentNode;
    Node<K, V> leftChild = currentNode.getLeftChild();
    if (leftChild != null) {
      result = findHeir(leftChild);
    }
    return result;
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> m) {

  }

  @Override
  public void clear() {
    root = null;
  }

  @Override
  public Set<K> keySet() {
    return null;
  }

  @Override
  public Collection<V> values() {
    return null;
  }

  @Override
  public Set<Entry<K, V>> entrySet() {
    return null;
  }

  public Node<K, V> getRoot() {
    return root;
  }

  public void printTree(Node<K, V> localRoot){
    if (localRoot!=null){
      printTree(localRoot.getLeftChild());
      System.out.print(localRoot.key + " ");
      printTree(localRoot.getRightChild());
    }
  }

  private static class Node<K extends Comparable<K>, V> {

    private K key;
    private V value;
    private Node<K, V> leftChild;
    private Node<K, V> rightChild;
    private Node<K, V> parent;

    public Node(K key, V value, Node<K, V> parent) {
      this.key = key;
      this.value = value;
      this.parent = parent;
    }

    public K getKey() {
      return key;
    }

    public void setKey(K key) {
      this.key = key;
    }

    public V getValue() {
      return value;
    }

    public void setValue(V value) {
      this.value = value;
    }

    public Node<K, V> getLeftChild() {
      return leftChild;
    }

    public void setLeftChild(Node<K, V> leftChild) {
      this.leftChild = leftChild;
    }

    public Node<K, V> getRightChild() {
      return rightChild;
    }

    public void setRightChild(Node<K, V> rightChild) {
      this.rightChild = rightChild;
    }

    public Node<K, V> getParent() {
      return parent;
    }

    public void setParent(Node<K, V> parent) {
      this.parent = parent;
    }
  }
}


